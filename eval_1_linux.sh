#!/bin/bash

if [ -e /tmp/out/verrou.lock ]
then
  rm /tmp/out/verrou.lock
  exit 22
fi

touch /tmp/out/verrou.lock

mkdir /tmp/in
mkdir /tmp/out
mkdir /tmp/intermediaire

cp -r /tmp/in/* /tmp/intermediaire
gzip -f /tmp/intermediaire/*
cp /tmp/intermediaire/* /tmp/out
rm -fr /tmp/intermediaire

rm /tmp/out/verrou.lock
